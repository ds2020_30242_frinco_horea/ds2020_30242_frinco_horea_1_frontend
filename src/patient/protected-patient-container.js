import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedPatientContainer = ({ component: Component, role, ...rest }) => {
    return (
        <Route {...rest} render={
            props => {
                if (role === 'patient') {
                    return <Component {...rest} {...props} />
                } else {
                    return <Redirect to={
                        {
                            pathname: '/unauthorized',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                }
            }
        } />
    )
}

export default ProtectedPatientContainer;