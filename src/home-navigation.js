import React from 'react'
import logo from './commons/images/icon4.png';
import Cookies from 'js-cookie';

import {
    Nav,
    Navbar,
    NavbarBrand,
    NavItem, Button
} from 'reactstrap';


class HomeNavigation extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return ( <HomeNavigationNavBar /> )
    }
}

function logout(){
    Cookies.remove("isLoggedIn");
    Cookies.remove("user");
    Cookies.remove("role");
    Cookies.remove('patientSelected');
    Cookies.remove('patientId');
}

const HomeNavigationNavBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"}/>
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
            </Nav>
            <NavItem>
                <Button href="/" onClick={logout} active>Logout</Button>
            </NavItem>
        </Navbar>
    </div>
);

export default HomeNavigation
