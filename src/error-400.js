import React from 'react';

const Error400 = () => {
    return (
        <div className="bg">
            <div id="sign-wrapper">
                <div id="hole1" className="hole"/>
                <div id="hole2" className="hole"/>
                <div id="hole3" className="hole"/>
                <div id="hole4" className="hole"/>
                <header id="header">
                    <h1>ERROR 400</h1>
                    <h1>Invalid request</h1>
                </header>
            </div>
        </div>
    )
}

export default Error400;