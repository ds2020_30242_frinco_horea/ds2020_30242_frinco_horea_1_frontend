import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row,
    Button,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Modal
} from 'reactstrap';
import * as API_CAREGIVERS from "./api/caregiver-api"
import Cookies from 'js-cookie'
import CaregiverPatientsTable from "./components/caregiver-patients-table";
import CaregiverPatientMedicationPlansTable from "./components/caregiver-patient-medication-plans-table";
import {Redirect} from "react-router-dom";
import HomeNavigation from "../home-navigation";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

const socket = new SockJS('http://localhost:8080/alert');
const stompClient = Stomp.over(socket);

class CaregiverContainer extends React.Component {
    constructor(props) {
        super(props);
        this.reloadMedicationPlansTable = this.reloadMedicationPlansTable.bind(this);
        this.fetchPatientMedicationPlan = this.fetchPatientMedicationPlan.bind(this);
        this.toggleMessageModal = this.toggleMessageModal.bind(this);
        this.showMessage = this.showMessage.bind(this);
        this.state = {
            username : Cookies.get('user'),
            selectedPatient: '',
            patientsTableData: [],
            patientsTableDataLoaded: false,
            medicationPlansTableData: [],
            medicationPlansTableDataLoaded: false,
            message: '',
            selectedMessageModal: false,
            errorStatus: 0,
            error: null
        };
    }

    showMessage(alert) {
        let caregiverUsername = Cookies.get("user");

        if (caregiverUsername === alert.caregiverUsername)
        {
            this.setState({
                message: alert.message,
                selectedMessageModal: true
            })
        }
    }

    toggleMessageModal() {
        this.setState({
            selectedMessageModal: !this.state.selectedMessageModal
        });
    }

    fetchCorrespondingPatients() {
        return API_CAREGIVERS.fetchCorrespondingPatients(this.state.username, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patientsTableData: result,
                    patientsTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchPatientMedicationPlan(patientUsername, patientName) {
        this.reloadMedicationPlansTable();
        return API_CAREGIVERS.fetchPatientMedicationPlan(patientUsername, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    medicationPlansTableData: result,
                    medicationPlansTableDataLoaded: true,
                    selectedPatient: patientName
                });
                console.log(this.state.medicationPlansTableData);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reloadMedicationPlansTable() {
        this.setState({
            medicationPlansTableData: [],
            medicationPlansTableDataLoaded: false,
            selectedPatient: ''
        });
        this.fetchCorrespondingPatients();
    }

    componentDidMount() {
        this.fetchCorrespondingPatients()

        stompClient.connect({}, (frame) => {
            console.log('Connected: ' + frame)
            stompClient.subscribe('/topic/alerts', (alert) => {this.showMessage(JSON.parse(alert.body))});
        })
    }

    render() {

        if (!Cookies.get('role')) {
            return (
                <Redirect to = "/"/>
            )
        }
        else {
            let role = Cookies.get('role');
            if (role !== 'caregiver') {
                return (
                    <Redirect to = "/"/>
                )
            }
        }

        return (
            <div>
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Patients table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                                {this.state.patientsTableDataLoaded && <CaregiverPatientsTable
                                    tableData = {this.state.patientsTableData}
                                    handleClick={this.fetchPatientMedicationPlan}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            {this.state.selectedPatient !== '' && <strong> {this.state.selectedPatient}'s medication plans </strong>}
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>

                                {this.state.medicationPlansTableDataLoaded && <CaregiverPatientMedicationPlansTable tableData = {this.state.medicationPlansTableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />   }
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.selectedMessageModal} toggle={this.toggleMessageModal} className={this.props.className}>
                    <ModalHeader toggle={this.toggleMessageModal}>Alert</ModalHeader>
                    <ModalBody>
                        {this.state.message}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleMessageModal}>Do Something</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )

    }
}

export default CaregiverContainer;
