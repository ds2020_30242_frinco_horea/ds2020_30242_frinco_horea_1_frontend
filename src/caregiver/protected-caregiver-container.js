import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedCaregiverContainer = ({ component: Component, role, ...rest }) => {
    return (
        <Route {...rest} render={
            props => {
                if (role === 'caregiver') {
                    return <Component {...rest} {...props} />
                } else {
                    return <Redirect to={
                        {
                            pathname: '/unauthorized',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                }
            }
        } />
    )
}

export default ProtectedCaregiverContainer;