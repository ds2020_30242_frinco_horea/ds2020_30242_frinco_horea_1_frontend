import React from 'react'
import logo from './commons/images/icon4.png';
import Cookies from 'js-cookie';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown,
    NavItem, Button
} from 'reactstrap';

const
    textStyle = {
        color: 'white',
        textDecoration: 'none'
    };

class NavigationBar extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {

        let role = 'default';
        if (Cookies.get('role')) role = Cookies.get('role');

        if (role === 'default') {
            return ( <DefaultNavBar /> )
        }
        else if (role === 'patient') {
            return ( <PatientNavBar logout={this.props.logout} /> )
        }
        else if (role === 'caregiver') {
            return ( <CaregiverNavBar logout={this.props.logout} /> )
        }
        else if (role === 'doctor') {
            return ( <DoctorNavBar logout={this.props.logout} /> )
        }
    }
}

const DefaultNavBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"}/>
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar direction="right">
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn More</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>
            <NavItem>
                <Button href="/login" active>Login</Button>
            </NavItem>
        </Navbar>
    </div>
);

const PatientNavBar = (props) => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"}/>
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar direction="right">
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink href="/patient">Patient</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>
            <NavItem>
                <Button onClick={props.logout} active>Logout</Button>
            </NavItem>
        </Navbar>
    </div>
);

const CaregiverNavBar = (props) => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"}/>
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar direction="right">
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink href="/caregiver">Caregiver</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>
            <NavItem>
                <Button onClick={props.logout} active>Logout</Button>
            </NavItem>
        </Navbar>
    </div>
);

const DoctorNavBar = (props) => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"}/>
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar direction="right">
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink href="/doctor/patients">Manage Patients</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctor/caregivers">Manage Caregivers</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctor/medication">Manage Medication</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctor/patientSelection">Add Medication Plan</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>
            <NavItem>
                <Button onClick={props.logout} active>Logout</Button>
            </NavItem>
        </Navbar>
    </div>
);

export default NavigationBar
