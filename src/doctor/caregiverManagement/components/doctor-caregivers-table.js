import React from "react";
import Table from "../../../commons/tables/table";

const TheTable = props => {
    const columns = [
        {
            Header: '',
            accessor: 'caregiverId',
            show: false,
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Birth date',
            accessor: 'birthDate',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Username',
            accessor: 'userr.username',
        },
        {
            Header: 'Password',
            accessor: 'userr.password',
        },
        {
            Header: 'Delete',
            Cell: cell => (<button onClick={()=>props.handleDelete(cell.original.userr.username)}>Delete caregiver</button>)
        },
        {
            Header: 'Update',
            Cell: cell => (
                <button onClick={() => props.handleUpdate (
                    cell.original.caregiverId,
                    cell.original.name,
                    cell.original.gender,
                    cell.original.birthDate,
                    cell.original.address,
                    cell.original.userr.username,
                    cell.original.userr.password)
                }>Update caregiver
                </button>)
        },
    ];

    return (
        <Table
            data={props.tableData}
            columns={columns}
            search={filters}
            pageSize={5}
        />
    )
}


const filters = [
    {
        placeholder: 'Search by name',
        accessor: 'name'
    }
];

class DoctorCaregiversTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return ( <TheTable tableData={this.state.tableData} handleDelete={this.props.handleDelete} handleUpdate={this.props.handleUpdate}/> )
    }
}

export default DoctorCaregiversTable;