import React from 'react';
import * as API_DOCTORS from "../api/doctor-api";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import DoctorCaregiversTable from "./components/doctor-caregivers-table";
import DoctorAddCaregiverForm from "./components/doctor-add-caregiver-form";
import DoctorUpdateCaregiverForm from "./components/doctor-update-caregiver-form";
import HomeNavigation from "../../home-navigation";

class DoctorCaregiversContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.deleteCaregiver = this.deleteCaregiver.bind(this);
        this.updateCaregiver = this.updateCaregiver.bind(this);
        this.toggleAddCaregiverForm = this.toggleAddCaregiverForm.bind((this));
        this.toggleUpdateCaregiverForm = this.toggleUpdateCaregiverForm.bind((this));
        this.state = {
            caregiversTableData: [],
            caregiversTableDataLoaded: false,
            selectedAddCaregiverForm: false,
            selectedUpdateCaregiverForm: false,
            updateData: [],
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchAllCaregivers();
    }

    fetchAllCaregivers() {
        let entityName = 'caregiver';
        return API_DOCTORS.fetchAll(entityName, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    caregiversTableData: result,
                    caregiversTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteCaregiver(username) {
        let entityName = 'caregiver';
        return API_DOCTORS.deleteByUsername(entityName, username, (result, status, err) => {
            if (result !== null && status === 200) {
                this.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    updateCaregiver(caregiverId, name, gender, birthDate, address, username, password) {
        let data = {
            caregiverId: caregiverId,
            name: name,
            gender: gender,
            birthDate: birthDate,
            address: address,
            username: username,
            password: password
        }
        this.setState( {
            updateData: data
        })
        console.log(data);
        this.toggleUpdateCaregiverForm();
    }

    toggleAddCaregiverForm() {
        this.setState({
            selectedAddCaregiverForm: !this.state.selectedAddCaregiverForm
        });
    }

    toggleUpdateCaregiverForm() {
        this.setState({
            selectedUpdateCaregiverForm: !this.state.selectedUpdateCaregiverForm
        });
    }

    reload() {
        this.setState({
            caregiversTableData: [],
            caregiversTableDataLoaded: false,
            selectedAddCaregiverForm: false,
            selectedUpdateCaregiverForm: false
        });
        this.fetchAllCaregivers();
    }

    render() {
        console.log(this.state.caregiversTableData);

        return (
            <div className="fullscreen">
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Caregivers table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                            {this.state.caregiversTableDataLoaded && <DoctorCaregiversTable
                                tableData = {this.state.caregiversTableData}
                                handleDelete={this.deleteCaregiver}
                                handleUpdate={this.updateCaregiver}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '2', offset: 0}}>
                        <Button color="primary" onClick={this.toggleAddCaregiverForm}>Add Caregiver</Button>
                    </Col>
                </Row>

                <Modal isOpen={this.state.selectedAddCaregiverForm} toggle={this.toggleAddCaregiverForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddCaregiverForm}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <DoctorAddCaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdateCaregiverForm} toggle={this.toggleUpdateCaregiverForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateCaregiverForm}> Update Caregiver: </ModalHeader>
                    <ModalBody>
                        <DoctorUpdateCaregiverForm data={this.state.updateData} reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default DoctorCaregiversContainer;
