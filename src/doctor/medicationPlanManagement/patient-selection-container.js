import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';
import * as API_DOCTORS from "../api/doctor-api"
import Cookies from 'js-cookie'
import DoctorPatientsTable from "./components/doctor-patients-table";
import {Redirect} from "react-router-dom";
import HomeNavigation from "../../home-navigation";

class PatientSelectionContainer extends React.Component {

    constructor(props) {
        super(props);
        this.selectPatient = this.selectPatient.bind(this)
        this.state = {
            username : Cookies.get('user'),
            patientsTableData: [],
            patientSelected: false,
            patientsTableDataLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchAllPatients();
    }

    fetchAllPatients() {
        let entityName = 'patient'
        return API_DOCTORS.fetchAll(entityName, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patientsTableData: result,
                    patientsTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    selectPatient(id) {
        Cookies.set('patientSelected', true);
        Cookies.set('patientId', id);
        this.setState({
            patientSelected: true
        })
    }

    render() {

        if (this.state.patientSelected === true)
        {
            return (<Redirect to = "/doctor/medicationSelection"/>)
        }
        else return (
            <div>
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Patients table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                            {this.state.patientsTableDataLoaded && <DoctorPatientsTable
                                tableData = {this.state.patientsTableData}
                                handleClick={this.selectPatient}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Card>
                    </Col>
                </Row>
            </div>
        )

    }
}

export default PatientSelectionContainer;