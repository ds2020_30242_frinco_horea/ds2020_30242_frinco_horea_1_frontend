import React from 'react';
import * as API_DOCTORS from "../api/doctor-api";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import DoctorMedicationTable from "./components/doctor-medication-table";
import DoctorAddMedicationForm from "./components/doctor-add-medication-form";
import DoctorUpdateMedicationForm from "./components/doctor-update-medication-form";
import HomeNavigation from "../../home-navigation";

class DoctorMedicationContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.deleteMedication = this.deleteMedication.bind(this);
        this.updateMedication = this.updateMedication.bind(this);
        this.toggleAddMedicationForm = this.toggleAddMedicationForm.bind((this));
        this.toggleUpdateMedicationForm = this.toggleUpdateMedicationForm.bind((this));
        this.state = {
            medicationTableData: [],
            medicationTableDataLoaded: false,
            selectedAddMedicationForm: false,
            selectedUpdateMedicationForm: false,
            collapseForm: false,
            updateData: [],
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchAllMedication();
    }

    fetchAllMedication() {
        let entityName = 'medication';
        return API_DOCTORS.fetchAll(entityName, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    medicationTableData: result,
                    medicationTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteMedication(name) {
        let entityName = 'medication';
        return API_DOCTORS.deleteByUsername(entityName, name, (result, status, err) => {
            if (result !== null && status === 200) {
                this.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    updateMedication(medicationId, name, sideEffects, dosage) {
        let data = {
            medicationId: medicationId,
            name: name,
            sideEffects: sideEffects,
            dosage: dosage,
        }
        this.setState( {
            updateData: data
        })
        console.log(data);
        this.toggleUpdateMedicationForm();
    }

    toggleAddMedicationForm() {
        this.setState({
            selectedAddMedicationForm: !this.state.selectedAddMedicationForm
        });
    }

    toggleUpdateMedicationForm() {
        this.setState({
            selectedUpdateMedicationForm: !this.state.selectedUpdateMedicationForm
        });
    }

    reload() {
        this.setState({
            medicationTableData: [],
            medicationTableDataLoaded: false,
            selectedAddMedicationForm: false,
            selectedUpdateMedicationForm: false
        });
        this.fetchAllMedication();
    }

    render() {
        console.log(this.state.medicationTableData);

        return (
            <div className="fullscreen">
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Medication table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                            {this.state.medicationTableDataLoaded && <DoctorMedicationTable
                                tableData = {this.state.medicationTableData}
                                handleDelete={this.deleteMedication}
                                handleUpdate={this.updateMedication}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '2', offset: 0}}>
                        <Button color="primary" onClick={this.toggleAddMedicationForm}>Add Medication</Button>
                    </Col>
                </Row>

                <Modal isOpen={this.state.selectedAddMedicationForm} toggle={this.toggleAddMedicationForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddMedicationForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <DoctorAddMedicationForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdateMedicationForm} toggle={this.toggleUpdateMedicationForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateMedicationForm}> Update Medication: </ModalHeader>
                    <ModalBody>
                        <DoctorUpdateMedicationForm data={this.state.updateData} reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default DoctorMedicationContainer;
