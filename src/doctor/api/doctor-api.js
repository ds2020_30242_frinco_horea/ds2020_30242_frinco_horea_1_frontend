import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoints = {
    patients: '/doctor/patients',
    caregivers: '/doctor/caregivers',
    medication: '/doctor/medication',
    medicationPlan: '/doctor/medicationPlan',
};

function determineEndpoint(entity) {
    let endpoint = ''
    if (entity === 'patient') {
        endpoint = endpoints.patients
    }
    else if (entity === 'caregiver') {
        endpoint = endpoints.caregivers
    }
    else if (entity === 'medication') {
        endpoint = endpoints.medication
    }
    else if (entity === 'medicationPlan') {
        endpoint = endpoints.medicationPlan
    }

    return endpoint;
}

function fetchAll(entityName, callback) {
    let endpoint = determineEndpoint(entityName);
    let request = new Request(HOST.backend_api + endpoint, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteByUsername(entityName, username, callback) {
    let endpoint = determineEndpoint(entityName);
    let request = new Request(HOST.backend_api + endpoint + "?name=" + username, {
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function post(entityName, instance, callback) {
    let endpoint = determineEndpoint(entityName);
    let request = new Request(HOST.backend_api + endpoint, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(instance)
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function update(entityName, instance, callback) {
    let endpoint = determineEndpoint(entityName);
    let request = new Request(HOST.backend_api + endpoint, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(instance)
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchAll,
    deleteByUsername,
    post,
    update,
};
